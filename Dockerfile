FROM php:8.1.22-apache

RUN mkdir /app

COPY ./docker-config/vhost.conf /etc/apache2/sites-available/000-default.conf

WORKDIR /app

COPY . /app

RUN cp .env.example .env \
    && chown -Rf www-data:www-data /app \
    && a2enmod rewrite

RUN apt update \
    && apt-get -y install git zip unzip libapache2-mod-rpaf zlib1g-dev libpng-dev libxml2-dev libcurl4-gnutls-dev curl libonig-dev \
    && docker-php-ext-install gd mysqli dom mbstring curl pdo pdo_mysql

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

RUN composer install \
    && php artisan key:generate \
    && composer require ramsey/uuid \
    && composer require doctrine/dbal \
    && php artisan config:clear

EXPOSE 80
EXPOSE 8000

CMD ["php", "artisan", "serve", "--host=0.0.0.0", "--port=8000"]
